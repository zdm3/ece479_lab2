## for ECE479 ICC Lab2 Part3

'''
*Main Student Script*
'''

# Your works start here

# Import packages you need here
from inception_resnet import InceptionResNetV1Norm
import numpy as np
import tensorflow as tf

# Create a model
model = InceptionResNetV1Norm()

# Verify the model and load the weights into the net
model.summary()
print("Len Layers + " + str(len(model.layers)))
model.load_weights("./weights/inception_keras_weights.h5")  # Has been translated from checkpoint

model.save("inception_model")

converter = tf.lite.TFLiteConverter.from_saved_model(
    '/Users/zachmizrachi/Desktop/ece479_lab2/lab2_sp23/code/inception_model', signature_keys=None, tags=None
)

inception_model = converter.convert()

with open('inception_model.tflite', 'wb') as f:
  f.write(inception_model)

print("Done")